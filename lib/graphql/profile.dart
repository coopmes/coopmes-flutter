class Profile {
  String auth(String login, String password) {
    return """
       {
         auth(input:{
            login: "$login"
            password: "$password"
          }), {
            token
          }
       }
    """;
  }

  String getProfile() {
    return """
      {
        getProfile {
          name,
          friends,
          notAllowedFriends,
          reqToAllowedFriends,
          _id
        }
      }
    """;
  }

  String register(String email, String name, String password) {
    return """
      mutation createUser(){
        createUser(input: {
          name: "$name",
          email: "$email",
          password: "$password"
        }) {
          _id, confirmToken
        }
      }
    """;
  }
}