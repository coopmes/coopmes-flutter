import 'package:flutter/material.dart';

final kDarkMaterialTheme = ThemeData.dark().copyWith(
    primaryColor: Colors.teal[300],
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
         primary: Colors.teal[300],
         onPrimary: Colors.black
      )
    ),
    appBarTheme: AppBarTheme(
      backgroundColor: Colors.black26,
    ),
);