// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Back" : MessageLookupByLibrary.simpleMessage("Back"),
    "Email" : MessageLookupByLibrary.simpleMessage("Email"),
    "Password" : MessageLookupByLibrary.simpleMessage("Password"),
    "Retr_password" : MessageLookupByLibrary.simpleMessage("Retry password"),
    "Username" : MessageLookupByLibrary.simpleMessage("Username"),
    "back_to_sign" : MessageLookupByLibrary.simpleMessage("Back to sign in"),
    "entr_email" : MessageLookupByLibrary.simpleMessage("Please enter email"),
    "entr_password" : MessageLookupByLibrary.simpleMessage("Please enter password"),
    "entr_retry_password" : MessageLookupByLibrary.simpleMessage("Please retry password"),
    "entr_retry_password_must_match" : MessageLookupByLibrary.simpleMessage("Passwords must match"),
    "entr_username" : MessageLookupByLibrary.simpleMessage("Please enter username"),
    "entr_valid_email" : MessageLookupByLibrary.simpleMessage("Enter valid email"),
    "error_login_or_password" : MessageLookupByLibrary.simpleMessage("Invalid login or password"),
    "error_this_email_exist" : MessageLookupByLibrary.simpleMessage("This email exist in the system"),
    "login" : MessageLookupByLibrary.simpleMessage("Login"),
    "password" : MessageLookupByLibrary.simpleMessage("Password"),
    "pls_confirm_email" : MessageLookupByLibrary.simpleMessage("We have sent you an email. To confirm registration."),
    "pls_confirm_reg" : MessageLookupByLibrary.simpleMessage("To confirm registration."),
    "pls_confirm_sent_email" : MessageLookupByLibrary.simpleMessage("We have sent you an email."),
    "pls_enter_email" : MessageLookupByLibrary.simpleMessage("Please enter email"),
    "register" : MessageLookupByLibrary.simpleMessage("Register"),
    "signin" : MessageLookupByLibrary.simpleMessage("Sign in")
  };
}
