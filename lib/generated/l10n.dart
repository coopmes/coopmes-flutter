// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Please enter email`
  String get pls_enter_email {
    return Intl.message(
      'Please enter email',
      name: 'pls_enter_email',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get register {
    return Intl.message(
      'Register',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `Sign in`
  String get signin {
    return Intl.message(
      'Sign in',
      name: 'signin',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Invalid login or password`
  String get error_login_or_password {
    return Intl.message(
      'Invalid login or password',
      name: 'error_login_or_password',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get Email {
    return Intl.message(
      'Email',
      name: 'Email',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get Username {
    return Intl.message(
      'Username',
      name: 'Username',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get Password {
    return Intl.message(
      'Password',
      name: 'Password',
      desc: '',
      args: [],
    );
  }

  /// `Retry password`
  String get Retr_password {
    return Intl.message(
      'Retry password',
      name: 'Retr_password',
      desc: '',
      args: [],
    );
  }

  /// `Back`
  String get Back {
    return Intl.message(
      'Back',
      name: 'Back',
      desc: '',
      args: [],
    );
  }

  /// `Back to sign in`
  String get back_to_sign {
    return Intl.message(
      'Back to sign in',
      name: 'back_to_sign',
      desc: '',
      args: [],
    );
  }

  /// `Please enter email`
  String get entr_email {
    return Intl.message(
      'Please enter email',
      name: 'entr_email',
      desc: '',
      args: [],
    );
  }

  /// `Please enter username`
  String get entr_username {
    return Intl.message(
      'Please enter username',
      name: 'entr_username',
      desc: '',
      args: [],
    );
  }

  /// `Please enter password`
  String get entr_password {
    return Intl.message(
      'Please enter password',
      name: 'entr_password',
      desc: '',
      args: [],
    );
  }

  /// `Please retry password`
  String get entr_retry_password {
    return Intl.message(
      'Please retry password',
      name: 'entr_retry_password',
      desc: '',
      args: [],
    );
  }

  /// `Passwords must match`
  String get entr_retry_password_must_match {
    return Intl.message(
      'Passwords must match',
      name: 'entr_retry_password_must_match',
      desc: '',
      args: [],
    );
  }

  /// `Enter valid email`
  String get entr_valid_email {
    return Intl.message(
      'Enter valid email',
      name: 'entr_valid_email',
      desc: '',
      args: [],
    );
  }

  /// `This email exist in the system`
  String get error_this_email_exist {
    return Intl.message(
      'This email exist in the system',
      name: 'error_this_email_exist',
      desc: '',
      args: [],
    );
  }

  /// `We have sent you an email. To confirm registration.`
  String get pls_confirm_email {
    return Intl.message(
      'We have sent you an email. To confirm registration.',
      name: 'pls_confirm_email',
      desc: '',
      args: [],
    );
  }

  /// `To confirm registration.`
  String get pls_confirm_reg {
    return Intl.message(
      'To confirm registration.',
      name: 'pls_confirm_reg',
      desc: '',
      args: [],
    );
  }

  /// `We have sent you an email.`
  String get pls_confirm_sent_email {
    return Intl.message(
      'We have sent you an email.',
      name: 'pls_confirm_sent_email',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}