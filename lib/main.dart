import 'package:coopmes/graphql_configuration.dart';
import 'package:coopmes/screens/chats/Chats.dart';
import 'package:coopmes/screens/confirmReg/ConfirmReg.dart';
import 'package:coopmes/screens/layouts/Mobile.dart';
import 'package:coopmes/screens/loading/loading.dart';
import 'package:coopmes/screens/login/login.dart';
import 'package:coopmes/screens/registration/registration.dart';
import 'package:coopmes/themes/darkMaterial.theme.dart';
import 'package:coopmes/themes/lightMaterial.theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'generated/l10n.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:localstorage/localstorage.dart';

void main() async {
  // We're using HiveStore for persistence,
  // so we need to initialize Hive.
  await initHiveForFlutter();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    GraphQlConfiguration configuration = GraphQlConfiguration();

    ValueNotifier<GraphQLClient> client = ValueNotifier(configuration.clientToQuery());

    return GraphQLProvider(
      client: client,
      child: AdaptiveTheme(
          light: kLightMaterialTheme,
          dark: kDarkMaterialTheme,
          initial: AdaptiveThemeMode.dark,
          builder: (theme, darkTheme) => MaterialApp(
            title: 'Coopmes',
            localizationsDelegates: [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: S.delegate.supportedLocales,
            theme: theme,
            initialRoute: '/',
            routes: {
              '/login': (context) => LoginScreen(),
              '/register': (context) => RegistrationScreen(),
              '/app': (context) => Chats(),
              '/confirm': (context) => ConfirmReg(),
            },
            home: LoadingScreen(),
          )),
    );
  }
}
