import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:localstorage/localstorage.dart';


class GraphQlConfiguration {
  GraphQLClient clientToQuery() {
    final LocalStorage storage = new LocalStorage('auth');

    final HttpLink _httpLink = HttpLink(
      'https://coopmes.online/backend/graphql',
    );

    final AuthLink _authLink = AuthLink(
      headerKey: 'Token',
      getToken: () => storage.getItem('token'),
    );

    final Link _link = _authLink.concat(_httpLink);

    return GraphQLClient(
      cache: GraphQLCache(
        store: HiveStore(),
      ),
      link: _link,
    );
  }
}


