import 'package:flutter/material.dart';

class ErrorContainer extends StatelessWidget {
  final String text;
  final bool open;

  const ErrorContainer({
    Key key,
    @required this.text,
    this.open = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        duration: Duration(milliseconds: 300),
        //height: 100,
        height: open ? 50 : 0,
        curve: Curves.fastOutSlowIn,
        decoration: BoxDecoration(
          color: Colors.red[400],
          borderRadius: BorderRadius.all(Radius.circular(5))
        ),
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.red[400]),
          ),
          child: Text(this.text, style: TextStyle(color: Colors.black)),
        )
    );
  }
}