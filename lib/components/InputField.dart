import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  final String label;
  final Function validator;
  final String type;
  final String name;
  final dynamic controller;
  final Function onSaved;
  final bool isShowError;
  final Function onChanged;

  InputField({
    this.label,
    this.validator,
    this.type,
    this.name,
    this.controller,
    this.onSaved,
    this.isShowError = false,
    this.onChanged
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
          labelText: label,
          alignLabelWithHint: true
          // errorStyle: this.isShowError ? null  : TextStyle(height: 0),
          // errorBorder: UnderlineInputBorder(
          //    borderSide: BorderSide(color: Colors.grey)
          // )
      ),
      obscureText: type == 'password',
      validator: validator,
      onSaved: onSaved,
      onChanged: onChanged,
    );
  }
}
