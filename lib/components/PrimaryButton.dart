import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final VoidCallback press;
  final dynamic style;
  final bool isLoading;

  const PrimaryButton({
    Key key,
    @required this.text,
    @required this.press,
    this.isLoading = false,
    this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return SizedBox(
        height: 36,
        width: double.infinity,
        child: ElevatedButton(
          style: style,
          onPressed: isLoading == false ? press : null,
          child: isLoading ? SizedBox(
            height: 24,
            width: 24,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              strokeWidth: 2,
            ),
          ) : Text(text),
        ),
      );
   }
}