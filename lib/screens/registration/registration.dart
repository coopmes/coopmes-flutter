import 'package:coopmes/components/InputField.dart';
import 'package:coopmes/components/PrimaryButton.dart';
import 'package:coopmes/graphql/profile.dart';
import 'package:coopmes/graphql_configuration.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../../generated/l10n.dart';

class RegistrationScreen extends StatefulWidget {
  RegistrationScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final retryPasswordController = TextEditingController();

  bool isLoading = false;
  bool isError = false;

  final query = Profile();
  final GraphQlConfiguration configuration = GraphQlConfiguration();

  Future<void> _showDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(text),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future _handlerRegister() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    setState(() {
      isLoading = true;
    });

    final GraphQLClient client = configuration.clientToQuery();

    QueryResult resp = await client.mutate(MutationOptions(
      document: gql(
        query.register(
          emailController.text,
          usernameController.text,
          passwordController.text,
        ),
      ),
    ));

    setState(() {
      isLoading = false;
    });

    if (resp.hasException) {
        print(resp.exception.toString());

      _showDialog(S.of(context).error_login_or_password);
    } else {
      Navigator.pushNamed(context, '/confirm');
    }
  }

  bool isValidEmailStr(String text) {
    List<String> arr = text.split('@');

    if (arr.length != 2) {
      return false;
    }

    List<String> arr2 = arr[1].split('.');

    if (arr2.length != 2) {
      return false;
    }

    return true;
  }

  void _handlerBackBtn() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: _formKey,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Card(
                  child: Container(
                    width: double.infinity,
                    constraints: BoxConstraints(minWidth: 200, maxWidth: 600),
                    padding: EdgeInsets.all(30),
                    child: Wrap(
                      runSpacing: 20,
                      children: [
                        InputField(
                          label: S.of(context).Email,
                          controller: emailController,
                          validator: (String value) {
                            if (value == null || value.isEmpty) {
                              return S.of(context).entr_email;
                            }

                            if (!isValidEmailStr(value)) {
                              return S.of(context).entr_valid_email;
                            }

                            return null;
                          },
                        ),
                        InputField(
                          label: S.of(context).Username,
                          controller: usernameController,
                          validator: (String value) {
                            if (value == null || value.isEmpty) {
                              return S.of(context).entr_username;
                            }
                            return null;
                          },
                        ),
                        InputField(
                          label: S.of(context).password,
                          controller: passwordController,
                          type: 'password',
                          validator: (String value) {
                            if (value == null || value.isEmpty) {
                              return S.of(context).entr_password;
                            }
                            return null;
                          },
                        ),
                        InputField(
                          label: S.of(context).Retr_password,
                          controller: retryPasswordController,
                          type: 'password',
                          validator: (String value) {
                            if (value == null || value.isEmpty) {
                              return S.of(context).entr_retry_password;
                            }

                            if (passwordController.text !=
                                retryPasswordController.text) {
                              return S
                                  .of(context)
                                  .entr_retry_password_must_match;
                            }

                            return null;
                          },
                        ),
                        PrimaryButton(
                          text: S.of(context).register,
                          isLoading: this.isLoading,
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.green[400],
                            ),
                          ),
                          press: _handlerRegister,
                        ),
                        Divider(height: 10),
                        PrimaryButton(
                          text: S.of(context).back_to_sign,
                          press: _handlerBackBtn,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
