import 'package:coopmes/components/BottomDrawer.dart';
import 'package:coopmes/components/PrimaryButton.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class Chats extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ChatsState();
  }
}

class _ChatsState extends State<Chats> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.black26,
        title: Text("Chats"),
      ),
      body: Center(
        child: PrimaryButton(
           text: 'Exit app',
           press: () {
             final LocalStorage storage = new LocalStorage('auth');
             storage.setItem('token', null);
             Navigator.pushReplacementNamed(context, '/');
           },
        ),
      ),
      bottomNavigationBar: Container(
        child: Text('sadasd'),
      ),
    );
  }
}
