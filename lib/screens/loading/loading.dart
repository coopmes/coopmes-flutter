import 'package:coopmes/graphql/profile.dart';
import 'package:coopmes/graphql_configuration.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'dart:async';

import 'package:localstorage/localstorage.dart';

class LoadingScreen extends StatefulWidget {
  LoadingScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  void initState() {
    final GraphQlConfiguration configuration = GraphQlConfiguration();
    final LocalStorage storage = new LocalStorage('auth');

    Future checkAuth() async {
      String token = storage.getItem('token');

      print(token);

      if (token == null) {
        Navigator.pushReplacementNamed(context, '/login');
      } else {
        final query = Profile();
        final GraphQLClient client = configuration.clientToQuery();

        try {
            QueryResult res = await client.query(QueryOptions(document: gql(query.getProfile())));

            print(res.data);

            if (res.data != null) {
              Navigator.pushReplacementNamed(context, '/app');
            } else {
              Navigator.pushReplacementNamed(context, '/login');
            }

        } catch(e) {
           print(e);
           Navigator.pushReplacementNamed(context, '/login');
        }
      }
    }

    storage.ready.then((_) => checkAuth());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Query(
  //     options: QueryOptions(document: gql(query.getProfile())),
  //     builder: (
  //       QueryResult result, {
  //       Refetch refetch,
  //       FetchMore fetchMore,
  //     }) {
  //       if (result.data == null) {
  //         return Scaffold(
  //           body: Center(
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: <Widget>[
  //                 CircularProgressIndicator(),
  //               ],
  //             ),
  //           ),
  //         );
  //       } else {
  //
  //         print(result);
  //
  //         return Container();
  //       }
  //     },
  //   );
  // }
}
