import 'package:coopmes/components/PrimaryButton.dart';
import 'package:flutter/material.dart';
import '../../generated/l10n.dart';

class ConfirmReg extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ConfirmRegState();
  }
}

class _ConfirmRegState extends State<ConfirmReg> {
  void handlerBackToSign() {
    Navigator.of(context).pushReplacementNamed('/login');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Image.asset("assets/images/car_moving.png",width:400,height:200),
            ),
            Container(
              padding: EdgeInsets.all(15),
              child: Column(
                children: [
                  Text(
                    S.of(context).pls_confirm_sent_email,
                    style: TextStyle(fontSize: 24),
                  ),
                  Text(
                    S.of(context).pls_confirm_reg,
                    style: TextStyle(fontSize: 24),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 200,
              child: PrimaryButton(
                text: S.of(context).back_to_sign,
                press: handlerBackToSign,
              ),
            )
          ],
        ),
      ),
    );
  }
}
