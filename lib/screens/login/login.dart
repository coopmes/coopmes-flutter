import 'package:coopmes/components/ErrorContainer.dart';
import 'package:coopmes/components/PrimaryButton.dart';
import 'package:coopmes/components/InputField.dart';
import 'package:coopmes/graphql/profile.dart';
import 'package:coopmes/graphql_configuration.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import '../../generated/l10n.dart';
import 'package:graphql/client.dart';

class Model {
  String login;
  String password;

  Model({this.login, this.password});
}

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  final loginController = TextEditingController();
  final passwordController = TextEditingController();

  Model model = Model();

  bool isLoginBtnEnable = false;
  bool isInvalidLogin = false;
  bool isLoading = false;
  String error = '';

  final query = Profile();

  final GraphQlConfiguration configuration = GraphQlConfiguration();
  final LocalStorage storage = new LocalStorage('auth');

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(this.error),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _handlerLoginIn() async {
    _formKey.currentState.save();

    setState(() {
      isInvalidLogin = false;
      isLoading = true;
    });

    final GraphQLClient client = configuration.clientToQuery();

    QueryResult resp = await client.query(
        QueryOptions(document: gql(query.auth(model.login, model.password))));

    if (resp.hasException) {
      print(resp.exception.toString());

      _showMyDialog();

      setState(() {
        isInvalidLogin = true;
        isLoading = false;
        error = resp.exception.toString();
      });
    } else {
      storage.setItem('token', resp.data['auth']['token']);
      Navigator.pushReplacementNamed(context, '/app');
    }
  }

  void _handlerRegister() {
    Navigator.pushNamed(context, '/register');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: _formKey,
          onChanged: () {
            setState(() {
              isLoginBtnEnable = _formKey.currentState.validate();
            });
          },
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Card(
                  child: Container(
                    width: double.infinity,
                    constraints: BoxConstraints(minWidth: 200, maxWidth: 600),
                    padding: EdgeInsets.all(30),
                    child: Wrap(
                      runSpacing: 20,
                      children: [
                        ErrorContainer(
                          open: isInvalidLogin,
                          text: S.of(context).error_login_or_password,
                        ),
                        InputField(
                          label: S.of(context).login,
                          controller: loginController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return '';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            model.login = value;
                          },
                        ),
                        InputField(
                          label: S.of(context).password,
                          type: 'password',
                          controller: passwordController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return '';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            model.password = value;
                          },
                        ),
                        PrimaryButton(
                          isLoading: isLoading,
                          text: S.of(context).signin,
                          press: isLoginBtnEnable ? _handlerLoginIn : null,
                        ),
                        Divider(height: 10),
                        PrimaryButton(
                          text: S.of(context).register,
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.green[400],
                            ),
                          ),
                          press: _handlerRegister,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
