import 'package:coopmes/components/PrimaryButton.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class MobileLayout extends StatefulWidget {
  MobileLayout({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MobileLayoutState createState() => _MobileLayoutState();
}

class _MobileLayoutState extends State<MobileLayout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'app',
            ),
            PrimaryButton(
              text: 'Exit',
              press: () {
                final LocalStorage storage = new LocalStorage('auth');
                storage.setItem('token', null);
                Navigator.pushNamed(context, '/');
              },
            )
          ],
        ),
      ),
    );
  }
}